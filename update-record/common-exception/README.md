### 常见异常-排查记录
### 更新记录链接
#### 2020年1月6日
- LanguageMetadataContributor  根据多语言特性动态生成多语言字段-异常
```text
 Error creating bean with name 'entityManagerFactory' defined in class path resource [org/springframework/boot/autoconfigure/orm/jpa/HibernateJpaConfiguration.class]: Invocation of init method failed; nested exception is java.lang.NullPointerException
 ...
 
 Caused by: java.lang.NullPointerException: null
 	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:325)
 	at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
 	at com.ibyte.common.util.ReflectUtil.classForName(ReflectUtil.java:212)
 	at com.ibyte.component.jpa.contributor.HibernatePropertyParser.buildSimpleValue(HibernatePropertyParser.java:164)
 	at com.ibyte.component.jpa.contributor.HibernatePropertyParser.buildElement(HibernatePropertyParser.java:134)
 	at com.ibyte.component.jpa.contributor.HibernatePropertyParser.parse(HibernatePropertyParser.java:53)
 	at com.ibyte.component.jpa.contributor.InterfaceMetadataContributor.readInterface(InterfaceMetadataContributor.java:75)
 	at com.ibyte.component.jpa.contributor.InterfaceMetadataContributor.contribute(InterfaceMetadataContributor.java:47)
 	at com.ibyte.component.jpa.contributor.LocalMetadataContributor.contribute(LocalMetadataContributor.java:23)
 	at org.hibernate.boot.model.process.spi.MetadataBuildingProcess.complete(MetadataBuildingProcess.java:284)
 	at org.hibernate.jpa.boot.internal.EntityManagerFactoryBuilderImpl.metadata(EntityManagerFactoryBuilderImpl.java:904)
 	at org.hibernate.jpa.boot.internal.EntityManagerFactoryBuilderImpl.build(EntityManagerFactoryBuilderImpl.java:935)
 	at org.springframework.orm.jpa.vendor.SpringHibernateJpaPersistenceProvider.createContainerEntityManagerFactory(SpringHibernateJpaPersistenceProvider.java:57)
 	at org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean.createNativeEntityManagerFactory(LocalContainerEntityManagerFactoryBean.java:365)
 	at org.springframework.orm.jpa.AbstractEntityManagerFactoryBean.buildNativeEntityManagerFactory(AbstractEntityManagerFactoryBean.java:390)
 	at org.springframework.orm.jpa.AbstractEntityManagerFactoryBean.afterPropertiesSet(AbstractEntityManagerFactoryBean.java:377)
 	at org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean.afterPropertiesSet(LocalContainerEntityManagerFactoryBean.java:341)
 	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.invokeInitMethods(AbstractAutowireCapableBeanFactory.java:1837)
 	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.initializeBean(AbstractAutowireCapableBeanFactory.java:1774)
 	... 17 common frames omitted
 Disconnected from the target VM, address: '127.0.0.1:59846', transport: 'socket'
```
![LanguageMetadataContributor](https://images.gitee.com/uploads/images/2020/0104/015540_ac5b55c1_1468963.png "LanguageMetadataContributor error.png")

#### 2020年1月9日
- ModuleLoader-问题修复，``design_element``启动无法加载到个个模块和拓展机制信息
![](https://images.gitee.com/uploads/images/2020/0109/185444_44f9bf5a_1468963.png "no_module_resource.png")
![](https://images.gitee.com/uploads/images/2020/0109/185507_b66b60fd_1468963.png "desgin_element.png")
